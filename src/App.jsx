import { useState } from 'react'

import './App.css'
import Button from './components/Button/Button'
import ModalImage from './components/Modal/ModalTypes/ModalImage'
import ModalText from './components/Modal/ModalTypes/ModalText'


function App() {
  
  const [isFirstModalOpen, setFirstModal] = useState(false)
  const [isSecondModalOpen, setSecondModal] = useState(false)
  
  const openFirstModal=()=>setFirstModal(true)
  const openSecondModal=()=>setSecondModal(true)
  
  const closeFirstModal=()=>setFirstModal(false)
  const closeSecondModal=()=>setSecondModal(false)
  return (
    <>
      <Button 
      type={'button'} 
      classNames={'button'} 
      onClick={openFirstModal} 
      >
      Open first modal
    </Button>

      <Button
      type={'button'} 
      classNames={'button'} 
      onClick={openSecondModal} 
      >
      Open second modal
    </Button>

    {
      isFirstModalOpen &&(
        <ModalImage
      className={'main__modal-image'}
      title={'Product Delete!'}
      text={'By clicking the “Yes, Delete” button, PRODUCT NAME will be deleted.'}
      closeWindow={closeFirstModal}
      />
      )
    }

    {
      isSecondModalOpen &&(
        <ModalText
        title={'Add Product "NAME"'}
        text={'Description for you product'}
        closeWindow={closeSecondModal}
        />
      )
    }
    </>
  )
}

export default App
