import ModalHeader from "../ModalHeader/ModalHeader";
import ModalBody from "../ModalBody/ModalBody";
import ModalClose from "../ModalClose/ModalClose";
import ModalFooter from "../ModalFooter/ModalFooter";
import ModalWrapper from "../ModalWrapper/ModalWrapper";
import Modal from "../Modal";

export default function ModalImage({ className, title, text, closeWindow })
{
    return(
        <ModalWrapper closeWindow={closeWindow}>
            <Modal>
                <ModalHeader>
                    <ModalClose onClick={closeWindow}></ModalClose>
                </ModalHeader>
                <ModalBody>
                    <div className={className}/>
                    <h2>{title}</h2>
                    <p>{text}</p>
                </ModalBody>
                <ModalFooter
                firstText={"NO, CANCEL"}
                secondaryText={"YES, DELETE"}
                firstClick={closeWindow}
                secondaryClick={closeWindow}
                >
                </ModalFooter>
            </Modal>
        </ModalWrapper>

    )
}