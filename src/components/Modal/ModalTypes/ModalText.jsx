import ModalHeader from "../ModalHeader/ModalHeader";
import ModalBody from "../ModalBody/ModalBody";
import ModalClose from "../ModalClose/ModalClose";
import ModalFooter from "../ModalFooter/ModalFooter";
import ModalWrapper from "../ModalWrapper/ModalWrapper";
import Modal from "../Modal";

export default function ModalText({ className, title, text, closeWindow })
{
    return(
        <ModalWrapper closeWindow={closeWindow}>
            <Modal>
                <ModalHeader>
                    <ModalClose onClick={closeWindow}></ModalClose>
                </ModalHeader>
                <ModalBody>
                        <h2>{title}</h2>
                        <p>{text}</p>
                </ModalBody>
                <ModalFooter
                firstText={"ADD TO FAVORITE"}
                firstClick={closeWindow}
                >
                </ModalFooter>
            </Modal>
        </ModalWrapper>

    )
}