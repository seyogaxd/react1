import '../Styles/ModalStyles.scss'
export default function ModalWrapper({children, closeWindow})
{
    function clickOutWindow(ev)
    {
        if (ev.target === ev.currentTarget) {
            closeWindow()
        }
    }

    return(
        <div className="main__modal-wrapper" onClick={clickOutWindow}>
            {children}
        </div>
    )
}